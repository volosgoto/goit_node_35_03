// console.log("Server");

// console.log(process.env.NODE_ENV);
// console.log(process.env.pizza);

// let args = ["sum", 10, 20, 50];

// let [operator, ...params] = args;
// console.log(operator);
// console.log(params);

// [].map((elem, idx, arr)=>{
// return изменений elem
// })

// [].reduce((acc, elem, idx, arr)=>{
//     return изменений acc
// },
// начальное значение аккумулятора
// )

// class Users {
//     constructor(name, age) {
//         // {}
//         // this
//         // return  {}

//         this.name = name;
//         this.age = age;
//     }

//     getInfo() {
//         console.log(this);
//     }

//     // getInfo () =>{}
// }

// let vova = new Users("Vova", 30);
// let sara = new Users("Sara", 25);
// let bob = new Users("Bob", 45);

// vova.getInfo();
// sara.getInfo();
// bob.getInfo();

class Users {
    static name = "Vova";
    static age = 25;

    // static setData(name, age) {
    //     console.log(this);
    //     console.log(name, age);
    // }
}

// Users.setData("Vova", 30);

console.log(Users.name);
console.log(Users.age);
